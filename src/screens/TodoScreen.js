import React, {useContext, useState} from 'react'
import {View, StyleSheet, Dimensions} from 'react-native'
import {FontAwesome, AntDesign} from '@expo/vector-icons'

import {THEME} from "../theme";
import {AppCard} from "../components/ui/AppCard";
import {EditModal} from "../components/EditModal";
import {AppTextBold} from "../components/ui/AppTextBold";
import {AppButton} from "../components/ui/AppButton";
import {ScreenContext} from "../context/screen/screenContext";
import {TodoContext} from "../context/todo/todoContext";

export const TodoScreen = () => {
    const [modal, setModal] = useState(false);
    const {removeTodo, todos, updateTodo} = useContext(TodoContext)
    const {changeScreen, todoId} = useContext(ScreenContext)

    const todo = todos.find(t => t.id === todoId)

    const saveHandler = (title) =>{
        updateTodo(todo.id, title)
        setModal(false)
    };

    return (
        <View>
            <EditModal
                visible={modal}
                onCancel={() => setModal(false)}
                value={todo.title}
                onSave={saveHandler}
            />
            <AppCard style={styles.card}>
                <AppTextBold style={styles.text}>{todo.title}</AppTextBold>
                <AppButton
                        onPress={() => setModal(true)}
                ><FontAwesome name={'edit'} size={20} />
                </AppButton>
            </AppCard>
            <View style={styles.buttons}>
                <View style={styles.button}>
                    <AppButton
                            color={THEME.GREY_COLOR}
                            onPress={() => changeScreen(null)}>
                        <AntDesign name={'back'} size={20} color={'#fff'}/>
                    </AppButton>
                </View>
                <View style={styles.button}>
                    <AppButton
                            color={THEME.DANGER_COLOR}
                            onPress={() =>
                                removeTodo(todo.id)
                                //   pressDelete
                            }>
                        <FontAwesome name={'remove'} size={20} />
                    </AppButton>
                </View>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    buttons: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    button: {
       // width: Dimensions.get('window').width / 3
        width: Dimensions.get('window').width > 400 ? 150 : 100
    },
    text: {
        fontSize: 20
    },
    card: {
        marginBottom: 20,
        padding: 15
    }
});