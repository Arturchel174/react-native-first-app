import React, {Component} from 'react'
import {View,StyleSheet, Platform } from 'react-native';
import {THEME} from "../theme";
import {AppTextBold} from "./ui/AppTextBold";


export const Navbar = props => {
    return(
        <View style={{...styles.navbar, ...Platform.select({
                ios: styles.navIos,
                android: styles.navAndroid
            })}}>
            <AppTextBold style={styles.text}>{props.title}</AppTextBold>
        </View>
    )
}

const styles = StyleSheet.create({
    navbar:{
        height: 70,
        alignItems:'center',
        justifyContent: 'center',

        paddingTop: 15
    },
    navAndroid:{
        backgroundColor: THEME.MAIN_COLOR,
    },
    navIos:{
        borderBottomColor: THEME.MAIN_COLOR,
        borderBottomWidth: 1
    },
    text:{
        color: Platform.OS === 'android' ? '#fff': THEME.MAIN_COLOR,
        fontSize: 20
    }
})